import spidev
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)

# Send a voltage impulse of 0.3 secs to the pin given as input.
def switchPin(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.3)
    GPIO.output(pin, GPIO.LOW)

# Effectively change the relays when the value of the dictionnary is changed.
def setRelays(data):
    initValue_fans = data["system"]["relays"]["fans"].value
    initValue_atomizer = data["system"]["relays"]["atomizer"].value

    while True:
        time.sleep(0.5)
        try:
            if data["system"]["relays"]["fans"].value != initValue_fans:
                if data["system"]["relays"]["fans"].value == 1:
                    switchPin(23)
                else:
                    switchPin(24)
                initValue_fans = data["system"]["relays"]["fans"].value

            if data["system"]["relays"]["atomizer"].value != initValue_atomizer:
                if data["system"]["relays"]["atomizer"].value == 1:
                    switchPin(27)
                else:
                    switchPin(22)
                initValue_atomizer = data["system"]["relays"]["atomizer"].value
        except:
            print("Error in the relays process.")
