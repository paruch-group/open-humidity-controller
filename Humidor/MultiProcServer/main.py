from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from multiprocessing.managers import ValueProxy
import datetime
import time
import os
import urllib2
from multiprocessing import Process, Manager, Value
import pandas as pd

import p_webServer
import p_WebAcquisiton
import p_pid
import p_pwm
import p_sensors
import p_flux
import p_relays
import p_potentiometer
import p_writeFile

if __name__ == '__main__':
    manager = Manager()

    sensor_inj = {"RH": manager.Value('d', 0), "T": manager.Value('d', 0), "DP": manager.Value('d', 0), "AH": manager.Value('d', 0), "ppmv": manager.Value('d', 0)}

    sensor_exh = {"RH": manager.Value('d', 0), "T": manager.Value('d', 0), "DP": manager.Value('d', 0), "AH": manager.Value('d', 0), "ppmv": manager.Value('d', 0)}

    sensor_cell = {"RH": manager.Value('d', 0), "T": manager.Value('d', 0), "DP": manager.Value('d', 0), "AH": manager.Value('d', 0), "ppmv": manager.Value('d', 0)}

    sensor_flows = {"wet": manager.Value('d', 0), "dry": manager.Value('d', 0)}

    sensor_web = {'pressure': manager.Value('d', 0), 'RH': manager.Value('d', 0), 'T': manager.Value('d', 0), 'DP': manager.Value('d', 0), 'last_update': manager.Value('c', " ")}

    sensors = {"inj": sensor_inj, "exh": sensor_exh, "cell": sensor_cell, "flows": sensor_flows, 'web': sensor_web}

    flows = {"wet": manager.Value('d', 0), "dry": manager.Value('d', 0)}

    params_PID = {"activated": manager.Value('i', 0), "setpoint": manager.Value('d', 0),
                  "proportional": manager.Value('d', 8), "integral": manager.Value('d', 1),
                  "derivative": manager.Value('d', 0), "control_signal": manager.Value('c', "sensors.inj.RH")}

    params_relays = {"fans": manager.Value('i', 0), "atomizer": manager.Value('i', 0)}

    params_digipot = {"resistance": manager.Value('d', 0)}

    params_activation = {"cell": manager.Value('i', 0), "exh": manager.Value('i', 1), "inj": manager.Value('i', 1), "wet": manager.Value('i', 1), "dry": manager.Value('i', 1), "PWM": manager.Value('i', 0), }

    params_pwm = {"cycle_time": manager.Value('d', 10), "on_ratio": manager.Value('d', 0.1)}

    sys_param = {"adcdac_busy": manager.Value('d', 0), "PID": params_PID, "relays": params_relays, "digipot": params_digipot, "activation": params_activation, "PWM": params_pwm}

    parameters = {"sensors": sensors, "flows": flows, "system": sys_param}

    info('main line')

    process_web = Process(target=p_webServer.WebServer, args=(parameters,))
    process_sensor = Process(target=p_sensors.SensorAcquisition, args=(parameters,))
    process_write = Process(target=p_writeFile.WriteFile, args=(parameters,))
    process_web_sensors = Process(target=p_WebAcquisiton.WebPressureAcquisition, args=(parameters,))
    process_flux = Process(target=p_flux.set_flux, args=(parameters,))
    process_pid = Process(target=p_pid.PIDControl, args=(parameters,))
    process_relays = Process(target=p_relays.setRelays, args=(parameters,))
    process_potentiometer = Process(target=p_potentiometer.setResistance, args=(parameters,))
    process_PWM = Process(target=p_pwm.PWMConfiguration, args=(parameters,))

    process_web.start()
    process_sensor.start()
    process_write.start()
    process_web_sensors.start()
    process_flux.start()
    process_pid.start()
    process_relays.start()
    process_potentiometer.start()
    process_PWM.start()

    process_web.join()
    process_sensor.join()
    process_write.join()
    process_web_sensors.join()
    process_flux.join()
    process_pid.join()
    process_relays.join()
    process_potentiometer.join()
    process_PWM.join()
