from multiprocessing.managers import ValueProxy
import datetime
import time
import f_parsing


# use the f_parsing functions to parse the dictionnary and write it in an .csv format.
def WriteFile(data):
    filename = str(datetime.datetime.now().date()) + '_' + str(datetime.datetime.now().time()).replace(':', '.')

    with open(filename + ".csv", "a") as f:
        h, d = f_parsing.parse_data("p", data)
        f.write("Time" + "\t" + "\t".join(h) + "\n")

    while True:
        time.sleep(1)
        with open(filename + ".csv", "a") as f:
            h, d = f_parsing.parse_data("p", data)
            f.write(datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S.%f') + "\t" + "\t".join(d) + "\n")
