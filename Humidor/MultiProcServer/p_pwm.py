import time

# Control the modulation of the vaporizer :
# The "on ratio" determines the ratio of the cycle time for which the vaporizer is on.

def PWMConfiguration(data):
    while True:
        try:
            if data["system"]["activation"]["PWM"].value == 1:
                data["system"]["relays"]["atomizer"].value = 1
                time.sleep(float(data["system"]["PWM"]["on_ratio"].value) * float(data["system"]["PWM"]["cycle_time"].value))
                data["system"]["relays"]["atomizer"].value = 0
                time.sleep((1-float(data["system"]["PWM"]["on_ratio"].value)) * float(data["system"]["PWM"]["cycle_time"].value))
            else:
                time.sleep(1)
        except:
            print("Error in the PWM process.")