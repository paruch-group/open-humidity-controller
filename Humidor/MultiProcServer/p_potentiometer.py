import spidev
import time

# Effectively change the values of the potentiometer when the value in the data dictionary is changed.
def setResistance(data):
    spi = spidev.SpiDev()
    spi.open(1, 0)
    spi.max_speed_hz = 1000000
    initRes = data["system"]["digipot"]["resistance"].value

    while True:
        time.sleep(0.5)
        # The potentiometer take an 8bit value, so the resistance value (0-10kOhm) must be converted in a 0,255 value.
        if data["system"]["digipot"]["resistance"].value != initRes:
            initRes = data["system"]["digipot"]["resistance"].value
            resistanceIn8Bit = int(min(255, data["system"]["digipot"]["resistance"].value * (255.0 / 10000.0)))
            spi.xfer([0, resistanceIn8Bit])
