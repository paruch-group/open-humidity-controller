from multiprocessing import Process, Manager, Value
from multiprocessing.managers import ValueProxy
import lxml.etree as et
import xmltodict
import lxml.etree as et
from json import loads, dumps
from collections import OrderedDict

def parse_data(path, data):
    ret_path = []
    ret_data = []
    if type(data) == dict:
        for key in data.keys():
            p, d = parse_data(path + "." + key, data[key])
            if type(p) == list:
                for e in p:
                    ret_path.append(e)
                for e in d:
                    ret_data.append(e)
            else:
                ret_path.append(p)
                ret_data.append(d)
    else:
        return path, str(data.value)
    return ret_path, ret_data


def dict_prepare(data, target):
    if type(data) == dict:
        for key in data.keys():
            target[key] = {}
            target[key] = dict_prepare(data[key], target[key])
    else:
        if type(data) == ValueProxy:
            return data.value
        else:
            return data
    return target


# Used to create an xml file from dictionnary
def dict2xml(d, name='data'):
    r = et.Element(name)
    return et.tostring(buildxml(r, d))

def buildxml(r, d):
    if isinstance(d, dict):
        for k, v in d.iteritems():
            s = et.SubElement(r, k)
            buildxml(s, v)
    elif isinstance(d, tuple) or isinstance(d, list):
        for v in d:
            s = et.SubElement(r, 'i')
            buildxml(s, v)
    elif isinstance(d, basestring):
        r.text = d
    else:
        r.text = str(d)
    return r

# Convert xml file to dict
def xml2dict(xml_file, xml_attribs=True):
    with open(xml_file, "rb") as f:  # notice the "rb" mode
        d = xmltodict.parse(f, xml_attribs=xml_attribs)
        return d


def buildxml(r, d):
    if isinstance(d, dict):
        for k, v in d.iteritems():
            s = et.SubElement(r, k)
            buildxml(s, v)
    elif isinstance(d, tuple) or isinstance(d, list):
        for v in d:
            s = et.SubElement(r, 'i')
            buildxml(s, v)
    elif isinstance(d, basestring):
        r.text = d
    else:
        r.text = str(d)
    return r

def xml2dict(xml_file, xml_attribs=True):
    with open(xml_file, "rb") as f:  # notice the "rb" mode
        d = xmltodict.parse(f, xml_attribs=xml_attribs)
        return d

def str2managerValue(data, target):
    manager = Manager()
    if type(data) == dict:
        for key in data.keys():
            target[key] = {}
            target[key] = str2managerValue(data[key], target[key])
    else:
        if type(data) == unicode:
            managerType = str(data.split("Value('")[1].split("',")[0])
            managerValue = str(data.split(",")[1].split(")")[0])
            if managerType == 'd':
                managerValue = float(managerValue)
            if managerType == 'i':
                managerValue = int(managerValue)
            if managerType == 'c' and str(managerValue.split("'")[1].split("'")[0]) != '':
                managerValue = str(managerValue.split("'")[1].split("'")[0])
            data = manager.Value(managerType, managerValue)
            return manager.Value(managerType, managerValue)
        else:
            return data
    return target