import f_humidor
import time

# Infinite loop :
# Check if the value of the flux has been changed and if the flux is activated.
# If so, set the voltage of the controller.

def set_flux(data):
    humidor = f_humidor.Humidor()
    initValue_dry = data["flows"]["dry"].value
    initValue_wet = data["flows"]["wet"].value
    while True:
        try:
            if data["flows"]["wet"].value != initValue_wet and data["system"]["activation"]["wet"].value == 1:
                while data["system"]["adcdac_busy"].value == 1:
                    time.sleep(0.05)
                data["system"]["adcdac_busy"].value = 1
                humidor.adcdac.set_dac_voltage(1, float(data["flows"]["wet"].value) / 100.0)
                data["system"]["adcdac_busy"].value = 0
                initValue_wet = data["flows"]["wet"].value
                print("Value of the flow wet successfully changed")

            if data["flows"]["dry"].value != initValue_dry and data["system"]["activation"]["dry"].value == 1:
                while data["system"]["adcdac_busy"].value == 1:
                    time.sleep(0.05)
                data["system"]["adcdac_busy"].value = 1
                humidor.adcdac.set_dac_voltage(2, float(data["flows"]["dry"].value) / 100.0)
                data["system"]["adcdac_busy"].value = 0
                initValue_dry = data["flows"]["dry"].value
                print("Value of the flow dry successfully changed.")
        except:
            print("Error while setting the flows values.")