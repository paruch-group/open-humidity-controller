import curses
import time
import datetime
import serial

class curses_screen:
    def __enter__(self):
        self.stdscr = curses.initscr()
        curses.cbreak()
        curses.noecho()
        self.stdscr.keypad(1)
        SCREEN_HEIGHT, SCREEN_WIDTH = self.stdscr.getmaxyx()
        return self.stdscr
    def __exit__(self,a,b,c):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()


with curses_screen() as stdscr:

    scr = curses.initscr()
    curses.halfdelay(1)
    curses.noecho()
    
    uniq_filename = "CellSim_" + str(datetime.datetime.now().date()) + '_' + str(datetime.datetime.now().time()).replace(':', '.')
    meas_fd = open(uniq_filename + ".csv", "w")
    meas_fd.write("Time\tTemperature\tHumidity\n")

    while True:
        curses.halfdelay(1)
        char = scr.getch()
                                  # or there is input to be handled
        scr.clear()               # Clears the screen
        if char != curses.ERR:    # This is true if the user pressed something
            try:
                if chr(char) == 'q':
                    meas_fd.close()
                    curses.endwin()
                    quit()
            except:
                pass
        else:
            scr.addstr(0, 0, "AFM Cell simulator")
            scr.addstr(1, 0, "====================")
            
            scr.addstr(16, 0, "Press Q to quit")
            scr.addstr(18, 0, " -> Recording data to " + uniq_filename + ".csv")
	    
	    with serial.Serial('/dev/ttyACM0', 9600) as ser:
	        line = ser.readline()
		splited_line = line.split()
		scr.addstr(2, 0, splited_line[1])
                scr.addstr(3, 0, splited_line[3])
		temperature = splited_line[1]
                humidity = splited_line[3]            
		
            meas_fd.write(str(datetime.datetime.now()) + "\t" + str(temperature) + "\t" + str(humidity) + "\n")
